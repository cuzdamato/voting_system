<?php

class ImagesController extends Zend_Controller_Action
{

    public function init()
    {
        // config variables. I use them throughout controller
        $this->imagesMapper = new Application_Model_ImagesMapper();
        $this->config = $this->getFrontController()->getParam('bootstrap');
        $this->imgConfig = $this->config->getOption('images');
        $this->cachePath = $this->imgConfig['cacheLink'] . $this->imgConfig['cachePrefix'];  
        $this->request = $this->getRequest();
        
        // count(rows in db)
        // i must know, when i can't get unique image from db
        $db = new Application_Model_ImagesMapper();
        $result = $db->fetchAll();
        Zend_Registry::set('rows', count($result));
        
        // i need variable to save id one image. 
        // at the beginning needs to draw two 
        // different pictures when you do not yet exist cookies
        $this->imageOne = 0;
        
        // we set the cookie 'counter' if isn't it
        // now we know if we have images that user doesn't rated
        if ($this->getRequest()->getCookie('counter') == null) {
            // why quantity ? this is start images
            setcookie('counter', $this->imgConfig['quantity'], time() + 3600, '/');
        }
        
        // switch context for ajax transmission
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('ajax', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        // variables for view
        $this->view->imagesPath = $this->imgConfig['link'];
        $this->view->imagesCache = $this->imgConfig['cacheLink'];
        
        // $imgFilenames will keep track of images
        $imgFilenames = array();
        
        if ($this->request->getCookie('counter') >= Zend_Registry::get('rows') - 1) {
            $alert = 1;
            $this->redirect('/default/images/summary/?alert=' . $alert);
        }
        
        // quantity is set in application.ini -> this value say, how many images we have on the site
        for($i = 0; $i < $this->imgConfig['quantity']; $i = $i + 1)
        {               
            $randomImage = $this->imagesMapper->getRandomRow();
            
            while($this->request->getCookie('id' . $randomImage->getId()) == $randomImage->getId() ||
                    $this->imageOne == $randomImage->getId())
            {
                $randomImage = $this->imagesMapper->getRandomRow();
            }

            $this->imageOne = $randomImage->getId();
            $imgFilenames[$i] = array(
                'id'        => $randomImage->getId(),
                'src'       => $this->imgConfig['link'] . $randomImage->getFilename()
            );
        }
        
        // variable provides view path to images
        $this->view->imgFilenames = $imgFilenames;
    }

    public function ajaxAction() 
    {        
        // variable from ajax call
        $post = $this->request->getPost();
        $clicked = $post['clicked'];
        $id0 = $post['id0'];
        $id1 = $post['id1'];
        
        // user gave voice to one of the two images, but
        // both are not available. Saves their id in cookies.
        setcookie('id' . $id0, $id0, time() + 3600, '/');
        setcookie('id' . $id1, $id1, time() + 3600, '/');
        
        // if user click on image -> update vote
        if ($clicked > 0) 
        {
            $db = $this->imagesMapper->getDbTable();
            $imageModel = new Application_Model_Images();
            $this->imagesMapper->find($clicked, $imageModel);
            $imageModel->addVote();

            $data = array(
                'filename'      => $imageModel->getFilename(),
                'added'         => $imageModel->getDateAdded(),
                'votes'         => $imageModel->getVotes()
            );

            $where = array(
                'id = ?'    => $clicked
            );

            $db->update($data, $where); 
        }
        
        /*
         *  logic
         */
        
        // are images in db, which user didn't voted ?
        if ($this->request->getCookie('counter') >= Zend_Registry::get('rows') - 1) {
            $alert = 1;
            $data['redirect'] = 'http://localhost/projects/voting_system/public/images/summary?alert=' . $alert;
            die(Zend_Json::encode($data));
        }
        
        // $imgFilenames will keep track of images
        $imgFilenames = array();
        
        // quantity is set in application.ini -> this value say, how many images we have on the site
        for($i = 0; $i < $this->imgConfig['quantity']; $i = $i + 1)
        {
            $randomImage = $this->imagesMapper->getRandomRow();
            
            /*
             * sometimes you will notice that the server response is faster than the setting of cookies. 
             * I don't know exactly why that is, but the introduction of an additional 
             * condition (id0, id1) solves the problem of delay (images don't repeat)
             */
            while($this->request->getCookie('id' . $randomImage->getId()) == $randomImage->getId() ||
                    $this->imageOne == $randomImage->getId() || $id0 == $randomImage->getId() || $id1 == $randomImage->getId())
            {
                $randomImage = $this->imagesMapper->getRandomRow();
            }
            
            $this->imageOne = $randomImage->getId();
            
            // if file don't exist, we must scaled it
            if (!file_exists($this->cachePath . $randomImage->getFilename())) {
                $classesImage = new Classes_Image();
                $classesImage->open($this->imgConfig['link'] . $randomImage->getFilename());
                $classesImage->scale(440, 600);
                $classesImage->save($this->cachePath . $randomImage->getFilename());
            }
        
            $imgFilenames['image' . $i] = array('id' => $randomImage->getId(), 'src' => $this->cachePath . $randomImage->getFilename());
        }
        
        // $counter say me, can i new images in db ? images, which user didn't voted
        $counter = $this->request->getCookie('counter');
        $counter = $counter + $this->imgConfig['quantity'];
        setcookie('counter', $counter, time() + 3600, '/');
        
        // send data 
        die(Zend_Json::encode($imgFilenames));
    }
    
    public function alertAction()
    {
    }
    
    public function summaryAction() 
    {
        $images = array();
        $result = $this->imagesMapper->fetchAll();
        
        foreach ($result as $key)
        {
            array_push($images, array(
                'src'       => $this->imgConfig['link'] . $key->getFilename(), 
                'cache'     => $this->imgConfig['cacheLink'] . '../thumb/', 
                'votes'     => $key->getVotes())
            );
        }

        $this->view->images = $images;
    }
}