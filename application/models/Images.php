<?php
// application/models/Images.php

class Application_Model_Images
{
    protected $_id;
    protected $_filename;
    protected $_date_added;
    protected $_votes;
    
    public function __construct(array $options = null)
    {
        if (is_array($options))
        {
            $this->setOptions($options);
        }
    }
    
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (($name == 'mapper') || !method_exists($this, $method)) 
        {
            throw new Exception('Invalid images property');
        }
        $this->method($value);
    }
    
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        
        foreach ($options as $key => $value)
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods))
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setFilename($filename)
    {
        $this->_filename = (string) $filename;
        return $this;
    }
    
    public function getFilename()
    {
        return $this->_filename;
    }
    
    public function setDateAdded($date_added)
    {
        $this->_date_added = (string) $date_added;
        return $this;
    }

    public function getDateAdded()
    {
        return $this->_date_added;
    }
    
    public function setVotes($votes)
    {
        $this->_votes = (int) $votes;
        return $this;
    }
    
    public function getVotes() 
    {
        return $this->_votes;
    }
    
    // add one vote
    public function addVote()
    {
        $this->_votes = $this->_votes + 1;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getId()
    {
        return $this->_id;
    }
}