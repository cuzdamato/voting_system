<?php

class Application_Model_ImagesMapper 
{
    protected $_dbTable;
    
    public function setDbTable($dbTable) 
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table date gateway provided');
        }
        
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function getDbTable() 
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Application_Model_DbTable_Images');
        }
        return $this->_dbTable;
    }
    
    public function find($id, Application_Model_Images $image)
    {
        $result = $this->getDbTable()->find($id);
        if (count($result) == 0) 
        {
            return;
        }
        
        $row = $result->current();
        $image->setId($row->id)
              ->setFilename($row->filename)
              ->setDateAdded($row->added)
              ->setVotes($row->votes);
    }
    
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = array();
        
        foreach ($resultSet as $row) 
        {
            $entry = new Application_Model_Images();
            $entry->setId($row->id)
                  ->setFilename($row->filename)
                  ->setDateAdded($row->added)
                  ->setVotes($row->votes);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function getRandomRow()
    {
        $db = $this->getDbTable();
        
        $result = $db->fetchRow($db->select()
                ->from('images')
                ->order('RAND()')
                ->limit('1'));
        
        $entry = new Application_Model_Images();
        
        $entry->setId($result->id)
              ->setFilename($result->filename)
              ->setDateAdded($result->added)
              ->setVotes($result->votes);
        
        return $entry;
    }
}

