<?php

class Zend_View_Helper_Image extends Zend_View_Helper_Abstract
{
    protected $_imageSrc;
    protected $_filename;
    protected $_cacheDir;
    protected $_newFilename;

    public function image($path, $cacheDir, $saveNamePrefix = 'scaled_', $newWidth = 440, $newHeight = 600)
    {
        $src = pathinfo($path);
        $this->_imageSrc = $path;
        $this->_filename = $src['basename'];
        
        $this->_cacheDir = $cacheDir;
        
        $image = new Classes_Image();
        $image->open($this->_imageSrc);
        $image->scale($newWidth, $newHeight);
        
        $this->_newFilename = $this->_cacheDir . $saveNamePrefix . $this->_filename;
        
        $image->save($this->_newFilename);
        $this->_render();
    }
    
    protected function _render() 
    {
        $html = '<img src="' . $this->view->baseUrl($this->_newFilename) . '">';
        echo $html;
    }
}