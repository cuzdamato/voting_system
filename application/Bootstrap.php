<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initRequest()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = new Zend_Controller_Request_Http();
        $front->setRequest($request);
        return $request;
    }
    
    protected function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();
        
        $view->doctype('HTML5');
        $view->headMeta()->setCharset('UTF-8');
    }
    
    protected function _initConfig() 
    {
        Zend_Registry::set('config', $this->getOptions());
    }
    
    protected function _initAutoloader()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace("Classes");

        return $autoloader;
    }
    
    protected function _initMedia() 
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap.min.css'))
                         ->appendStylesheet($view->baseUrl('css/metro-bootstrap.min.css'))
                         ->appendStylesheet($view->baseUrl('css/jquery-ui.min.css'))
                         ->appendStylesheet($view->baseUrl('css/main.css'));
        
        $view->headScript()->prependFile($view->baseUrl('js/jquery-2.1.1.min.js'));
    }
}

