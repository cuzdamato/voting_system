<?php

class Classes_Image {
    protected $_filename = '';
    protected $_image = '';
    protected $_width = '';
    protected $_height = '';
    protected $_mime_type = '';
    protected $_saveIn = '';
    
    const IMAGETYPE_GIF = 'image/gif';
    const IMAGETYPE_JPEG = 'image/jpeg';
    const IMAGETYPE_PNG = 'image/png';
    const IMAGETYPE_JPG = 'image/jpg';
    
    public function open($filename) 
    {
        $this->_filename = $filename;
        $this->_setInfo();
        
        switch($this->_mime_type) 
        {
            case self::IMAGETYPE_GIF:
                $this->_image = imagecreatefromgif($this->_filename);
                break;
            case self::IMAGETYPE_JPEG:
            case self::IMAGETYPE_JPG:
                $this->_image = imagecreatefromjpeg($this->_filename);
                break;
            case self::IMAGETYPE_PNG:
                $this->_image = imagecreatefrompng($this->_filename);
                break;
            default:
                throw new Exception('Image extension is invalid or not supported.');
        }
        return $this;
    }
    
    protected function _output($saveIn = null)
    {
        switch ($this->_mime_type)
        {
            case self::IMAGETYPE_GIF:
                return imagegif($this->_image, $saveIn);
            case self::IMAGETYPE_JPEG:
            case self::IMAGETYPE_JPG:
                return imagejpeg($this->_image, $saveIn, 100);
            case self::IMAGETYPE_PNG:
                return imagepng($this->_image, $saveIn, 100);
            default:
                throw new Exception('Image cannpt be created.');
        }
    }
    
    public function save($saveIn)
    {
        $this->_saveIn = $saveIn;
        return $this->_output($saveIn);
    }
    
    public function linkToSaved()
    {
        return $this->_saveIn;
    }
    
    
    protected function _setInfo() 
    {
        $imageSize = @getimagesize($this->_filename);
        
        // check whether the image is correct
        if (!$imageSize)
        {
            throw new Exception('Could not extract image size.');
        } 
        elseif ($imageSize[0] == 0 || $imageSize[1] == 0) 
        {
            throw new Exception('Image has dimension of zero.');
        }
        
        $this->_width = $imageSize[0];
        $this->_height = $imageSize[1];
        $this->_mime_type = $imageSize['mime'];
    }
    
    public function getWidth()
    {
        return $this->_width;
    }
    
    public function getHeight()
    {
        return $this->_height;
    }
    
    public function scale($newWidth, $newHeight)
    {
        // image is smaler than the target -> return 
        if ($this->_width < $newWidth && $this->_height < $newHeight)
        {
            return $this;
        }
        
        // original aspect ratio of the image
        $originalRatio = $this->_width / $this->_height;
        
        if ($newWidth / $newHeight > $originalRatio)
        {
            $newWidth = $newHeight * $originalRatio;
        } 
        else 
        {
            $newHeight = $newWidth / $originalRatio;
        }
        
        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($newImage, $this->_image, 0, 0, 0, 0, $newWidth, $newHeight, $this->getWidth(), $this->getHeight());
        
        $this->_image = $newImage;
        return $this;
    }
}