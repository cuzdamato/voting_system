<?php

// scripts/load.mysql.php

/*
 * Script for creating and loading database
 */

require_once 'helpers.inc.php';

// Initialize the application path, environment and autoloading
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', 'production');

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/../library',
     get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

// Define some CLI options
$getopt = new Zend_Console_Getopt(array(
    'withdata|w'    => 'Load database with data from folder',
    'help|h'        => 'Help -- usage message',
));

try {
    $getopt->parse();
} catch (Zend_Console_Getopt_Exception $e) {
    // Bad options 
    echo $e->getUsageMessage();
    return false;
}

// If help requested, report usage message
if ($getopt->getOption('h'))
{
    echo $getopt->getUsageMessage();
    return true;
}

// Initialize valuse based on presence or absence of CLI options
$withdata   = $getopt->getOption('w');

// Inizialize Zend_Application
$application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application.ini'
);

// Initialize and retrieve DB resource
$bootstrap = $application->getBootstrap();
$bootstrap->bootstrap('db');
$dbAdapter = $bootstrap->getResource('db');

// Check to see if we have a connection to database
$options = $bootstrap->getOption('resources');
$images = $bootstrap->getOption('images');

$params = array (
    'host'          => $options['db']['params']['host'],
    'username'          => $options['db']['params']['username'],
    'password'          => $options['db']['params']['password'],
    'dbname'          => $options['db']['params']['dbname']
);

$db = Zend_Db::factory('PDO_Mysql', $params);

try {
    $db->getConnection();
    echo PHP_EOL;
    echo "Database connection has been established";
    echo PHP_EOL;
} catch (Zend_Db_Adapter_Exception $e) {
    echo 'An ERROR HAS OCCURED:' . PHP_EOL;
    echo $e->getMessage() . PHP_EOL;
    return false;
}

try 
{	
    $schemaSql = file_get_contents(dirname(__FILE__) . '/schema.mysql.sql');
    $db->getConnection()->exec($schemaSql);

    echo PHP_EOL;
    echo "Table of images created.";
    echo PHP_EOL;

    if ($withdata) {
        $counter = 0;
        $images = filenames($images['path']);
        
        for ($i = 0; $i < count($images); $i = $i + 1)
        {
            $values = array(
                'filename'      => $images[$i],
                'added'         => date("Y-m-d H:i:s")
            );
            $query = "SELECT * FROM images WHERE filename='$images[$i]' LIMIT 1";
            $result = $db->query($query);
            
            $row = $result->fetch();
            if ($row['filename'] == $images[$i]) {
                continue;
            } else {
                if ($db->insert('images', $values))
                    $counter++;
            }
        }
        if ($counter) {
            echo PHP_EOL;
            echo "Data loaded. New images: " . $counter;
            echo PHP_EOL;
        }
    }

} 
catch (Exception $e) 
{
    echo 'An ERROR HAS OCCURED:' . PHP_EOL;
    echo $e->getMessage() . PHP_EOL;
    return false;
}

// generally speaking, this script will be run from the command line
return true;