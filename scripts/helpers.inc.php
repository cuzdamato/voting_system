<?php

// -----------------------------------------------------------------------------
function filenames($path) 
{
    $filenames = array();
    $dimg = opendir($path); 

    while ($imgfile = readdir($dimg)) 
    {
        if ($imgfile == '.' || $imgfile == '..')
            continue;

        $filenames[] = $imgfile;
    }
    
    return $filenames;
}
// -----------------------------------------------------------------------------